<?php

declare (strict_types = 1);

namespace App\Infrastructure\Db\User;

use App\Application\Query\User\{
    UserQuery, UserViewModel
};

class DbUserQuery extends UserQuery
{
    public function getById(int $id): ?UserViewModel
    {
        return new UserViewModel(1, 'Some Name');
    }

    public function getAll(): array
    {
        return [];
    }
}