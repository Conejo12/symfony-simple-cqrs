<?php 

declare (strict_types = 1);

namespace App\UI\Http;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Application\CommandBus;
use App\Application\Command\User\CreateUserCommand;

class DefaultController extends Controller
{
    public function index(Request $request, CommandBus $bus): Response
    {
        var_dump($bus);exit;
        $bus->handle(new CreateUserCommand(1, 'Asd'));

        return new Response();
    }
}