<?php

declare (strict_types = 1);

namespace App\Application;

use App\Application\Command\{
    ICommand, ICommandHandler
};

class CommandLocator
{
    /** var array */
    private $relationships;

    public function __construct(array $relationships)
    {
        $this->relationships = $relationships;
    }

    public function hasHandler(ICommand $command): bool
    {
        return isset($this->relationships[get_class($command)]);
    }

    public function getHandler(ICommand $command): ICommandHandler
    {
        return $this->relationships[get_class($command)];
    }
}
