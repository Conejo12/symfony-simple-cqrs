<?php

declare (strict_types = 1);

namespace App\Application\Command;

interface ICommandHandler
{
    public function handle(ICommand $command): void;
}