<?php

declare (strict_types = 1);

namespace App\Application\Command\User;

use App\Application\Command\{
    ICommandHandler, ICommand
};

class CreateUserHandler implements ICommandHandler
{
    public function handle(ICommand $command): void
    {
        echo $command->getId(), ' - ', $command->getName();
    }
}