<?php

declare (strict_types = 1);

namespace App\Application\Query\User;

abstract class UserQuery implements IQuery
{
    abstract public function getById(int $id): ?UserViewModel;
    abstract public function getAll(): array;
}