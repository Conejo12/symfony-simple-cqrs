<?php

declare (strict_types = 1);

namespace App\Application;

use App\Application\Command\ICommand;
use App\Application\Exception\HandlerNotFoundException;

class CommandBus
{
    /** var CommandLocator */
    private $locator;

    public function __construct(CommandLocator $locator)
    {
        $this->locator = $locator;
    }

    public function handle(ICommand $command): void
    {
        $this->assertHandler($command);

        $handler = $this->locator->getHandler($command);
        $handler->handle($command);
    }

    /**
     * @throws HandlerNotFoundException
     */
    private function assertHandler(ICommand $command): void
    {
        if (!$this->locator->hasHandler($command)) {
            throw new HandlerNotFoundException('Handler not found for ' . get_class($command), 400);
        }
    }
}
